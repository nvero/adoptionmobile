import React, { useState } from 'react';
import { Text, View, StyleSheet, Modal, Pressable, Image } from 'react-native';

const styles = StyleSheet.create({
    center: {
      alignItems: 'center'
    },
      centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
    },
    modalView: {
      margin: 20,
      backgroundColor: "#e6e6fa",
      borderRadius: 20,
      padding: 35,
      alignItems: "center",
      shadowColor: "#ff69b4",
      shadowOffset: {
        width: 0,
        height: 10
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    button: {
      borderRadius: 10,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    },
    tinyLogo: {
        width: 50,
        height: 50
    }
})
  

const Greeting = (props) => {
  return (
    <View style={styles.center}>
      <Text>Hello {props.name}!</Text>
    </View>
  );
}

const LotsOfGreetings = () => {
const [modalVisible, setModalVisible] = useState(false);
//const numeros = [1, 2, 3, 4, 5];
const names = ['bianca', 'tania', 'giuliana', 'vero', 'rosa'];
//const listaElementos = numeros.map((numero) => <li>{numero}</li>);
//const listaNames = names.map((name) => <li>{name}</li>)
  return (
    <View style={[styles.center, {top: 50}]}>
 
   <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
      
      <View style={styles.modalView}>
      <Text>Hello</Text>
      <Text>Lista de nombres</Text>
      <Image
        style={styles.tinyLogo}
        source={{
          uri: 'https://reactnative.dev/img/tiny_logo.png',
        }}>
      </Image>
       { names.map((name) => <Greeting name={name}/>) }
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Hide Modal :(</Text>
            </Pressable>
      </View>
      </Modal>
      
    
      <Pressable
        style={[styles.button, styles.buttonOpen]}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.textStyle}>Hace click!</Text>
      </Pressable>
 
    </View>
  );
};

export default LotsOfGreetings;