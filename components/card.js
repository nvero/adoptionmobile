import React from 'react';
import { Text, View, StyleSheet, Modal, Pressable, Image } from 'react-native';


const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "vertical",
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        textAlign: "center",
        borderRadius: 20,
        height: 500,
        width: 500,
        backgroundColor: "aqua",
        shadowColor: "#dc143c",
        shadowOffset: {
            width: 0,
            height: 10
        },
    },
    tinyLogo: {
        width:200,
        height:200
        // tintColor: "pink",
        // resizeMode: "contain"
    },
    texto: {
        justifyContent: 'center',
        backgroundcolor: 'green'
    }
})

const ShowImage = () => {

    return( 
        <View style={styles.container}>
            <Text style={styles.texto}>Imagen de React</Text>
            <Image  style={styles.tinyLogo}
                    source={{
                    uri: 'https://reactnative.dev/img/tiny_logo.png',
            }}>
            </Image>
        </View>
    );
}

export default ShowImage;