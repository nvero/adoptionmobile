import React from 'react';
import { Text, View, StyleSheet, Modal, Pressable, Image } from 'react-native';

const listDogs = [
    {
        name: 'Bandido',
        age: '1',
        image: require('../assets/photo1.jpg')
    },
    {
        name: 'Mayte',
        age: '1',
        image: require('../assets/photo2.jpg')
    },
    {
        name: 'Charly',
        age: '1',
        image: require('../assets/photo3.jpg')
    },
    {
        name: 'Capitan',
        age: '1',
        image: require('../assets/photo4.jpg')
    }

];

const listCats = [
    {
        name: 'Felix',
        age: '5',
        image: require('../assets/photo-cat1.jpg')
    },
    {
        name: 'Narciso',
        age: '6',
        image: require('../assets/photo-cat2.jpg')
    },
    {
        name: 'Figaro',
        age: '7',
        image: require('../assets/photo-cat3.jpg')
    },
    {
        name: 'Gatsby',
        age: '8',
        image: require('../assets/photo-cat4.jpg')
    }

];


const listAnimal = [
    {
        name: 'Bandido',
        age: '1',
        image: require('../assets/photo1.jpg')
    },
    {
        name: 'Mayte',
        age: '1',
        image: require('../assets/photo2.jpg')
    },
    {
        name: 'Charly',
        age: '1',
        image: require('../assets/photo3.jpg')
    },
    {
        name: 'Capitan',
        age: '1',
        image: require('../assets/photo4.jpg')
    },
    {
        name: 'Felix',
        age: '5',
        image: require('../assets/photo-cat1.jpg')
    },
    {
        name: 'Narciso',
        age: '6',
        image: require('../assets/photo-cat2.jpg')
    },
    {
        name: 'Figaro',
        age: '7',
        image: require('../assets/photo-cat3.jpg')
    },
    {
        name: 'Gatsby',
        age: '8',
        image: require('../assets/photo-cat4.jpg')
    }
];

const SelectorGalery = (props) => {
    if ('cat' === props.title) {
        return (<View style={styles.container}>
            {listCats.map( (cat) => <View style={styles.card}> 
                                        <Image style={styles.img} source={cat.image}></Image>
                                        <Text>{cat.name}</Text>
                                        <Text>{cat.age}</Text>
                                    </View>)
            }
        </View>
        );
    } else {
        return (
            <View style={styles.container}>
                {listDogs.map( (dog) => <View style={styles.card}> 
                                            <Image style={styles.img} source={dog.image}></Image>
                                            <Text>{dog.name}</Text>
                                            <Text>{dog.age}</Text>
                                        </View>)
                }
            </View>
        );
    }

}

const ShowGalery = (props) => {

    return (
        <View>
            <SelectorGalery title={props.title}></SelectorGalery>
        </View>
    );

}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'pink',
        margin: 20,
        padding:30
    },
    card: {
        shadowColor:'#a9a9a9',
        weight: 100,
        height: 100,
        padding: 30
    },
    img: {
        weight: 50,
        height: 50,
    }

});

export default ShowGalery;