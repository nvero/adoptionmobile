import React, {useState} from 'react';
import { Text, View, StyleSheet, Modal, Pressable, Image, CheckBox } from 'react-native';
import ShowGalery from '../components/ShowGalery.js';

const CheckComponent = (props) => {
    const [isSelected, setSelection] = useState(false);
    return(
        <View>
            <CheckBox
                value={isSelected}
                onValueChange={setSelection}
            ></CheckBox>
            {/* <Text>eleccion: {props.title}</Text> */}
            <Text> {isSelected? <ShowGalery title={props.title}></ShowGalery>: '' }</Text>
        </View>
    );
}

export default CheckComponent;