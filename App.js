import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import ShowImage from './components/card.js';
import CheckComponent from './components/CheckComponent';

import LotsOfGreetings from './components/modal.js';
import SelectorColour from './components/SelectorColour.js';

export default function App() {
  
  return (
    <>
    <View style={styles.container}>
      <View style={styles.card}>
      <Text><h2>primera vista!</h2></Text>
        <Text>Open up App.js to start working on your app!</Text>
        <StatusBar style="auto" />
        {/* <LotsOfGreetings></LotsOfGreetings> */}
      </View> 


      <View style={styles.card}>
        <Text><h2>segunda vista!</h2></Text>
        <LotsOfGreetings></LotsOfGreetings>
      </View>

      {/* <View style={styles.container}>
        <Text>cuarta vista!</Text>
        <SelectorColour></SelectorColour>
      </View> */}

      <View style={styles.card}> 
        <Text><h2>tercera vista!</h2></Text>
        <View>

          <Text>Cats</Text>
          <CheckComponent title={'cat'}></CheckComponent>

          <Text>Dogs</Text>
          <CheckComponent title={'dog'}></CheckComponent>

        </View>
      </View>
    </View>


    </>
    
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    // width: 1000,
    // height: 1000,
    padding: 30,
    backgroundColor: 'pink'
  },
  card: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    width: 1000,
    height: 500,
    padding: 30,
    margin: 50,
    shadowColor: '#e6e6fa',
    shadowOffset: {
      height: 10,
      width: 10
    },
    borderRadius: 20,
    backgroundColor: '#f8f8ff'
  }
});
